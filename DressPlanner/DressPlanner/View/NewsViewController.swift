//
//  NewsViewController.swift
//  DressPlanner
//
//  Created by Digital House on 25/11/19.
//  Copyright © 2019 Flavia Arsuffi. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class NewsViewController: UIViewController {
    
    @IBOutlet weak var newsTableView: UITableView!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var dayWeek: UILabel!
  
        var news: News?
        
        let dateFormatter = DateFormatter()
        
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.title = "Notícias"
            
            self.newsTableView.delegate = self
            self.newsTableView.dataSource = self
            
            self.dayWeek.text = getTodayWeekDay()
            
            self.alamofire()
            
            self.dateFormatter.locale = Locale(identifier: "pt_BR")
            
            self.dateLabel.text = DateFormatter.localizedString(from: Date(), dateStyle: DateFormatter.Style.long, timeStyle: DateFormatter.Style.none)
            
        }
        
        func getTodayWeekDay()-> String{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            let weekDay = dateFormatter.string(from: Date())
            return weekDay
        }
        
        
        func alamofire() {
            
            let param = [
                "country": "br",
                "apiKey": "4da149a7ebc64e8b87cacf42ba8c49a1"
            ]
            Alamofire.request("https://newsapi.org/v2/top-headlines", parameters: param).responseJSON { response in
                
                if let data = response.data {
                    let news = try? JSONDecoder().decode(News.self, from: data)
                    self.news = news
                    self.newsTableView.reloadData()
                }
                
            }
        }
        
    }

    extension NewsViewController: UITableViewDelegate, UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return news?.articles?.count ?? 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell: NewsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as? NewsTableViewCell
            
            cell?.labelViewCell?.text = news?.articles?[indexPath.row].title ?? ""
            cell?.imageViewCell?.sd_setImage(with: URL(string: news?.articles?[indexPath.row].urlToImage ?? ""), completed: nil)
            return cell ?? UITableViewCell()
            
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            let urlStr = news?.articles?[indexPath.row].url
            UIApplication.shared.open(URL(string:urlStr ?? "")!, options: [:], completionHandler: nil)
            
        }
        
    }
