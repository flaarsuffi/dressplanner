//
//  CatalogMenuBar.swift
//  DressPlanner
//
//  Created by Flavia Arsuffi on 23/10/19.
//  Copyright © 2019 Flavia Arsuffi. All rights reserved.
//

import UIKit

protocol CatalogMenuBarDelegate: NSObject {
    func dressClick()
    func upperClick()
    func lowerClick()
    func shoesClick()
}
class CatalogMenuBar: UIView {
    
    weak var delegate: CatalogMenuBarDelegate?
    
    var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    let cellId = "cellId"
    let menuCellIconNames = ["baseball-cap", "shirt", "pants", "shoes"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.allowsSelection = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(menuBarCell.self, forCellWithReuseIdentifier: "cellId")
        
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        collectionView.backgroundColor = .clear
        collectionView.isScrollEnabled = false
        collectionView.bounces = false
        let selectedIndexPath = NSIndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath as IndexPath, animated: false, scrollPosition: .centeredVertically)
    }
    
    required init?(coder: NSCoder) {
        fatalError("has not been implemented")
    }
}

class menuBarCell: UICollectionViewCell {
    
    let cellImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "baseball-cap")?.withRenderingMode(.alwaysTemplate)
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = #colorLiteral(red: 0.3694694042, green: 0.2306925356, blue: 0.6512844563, alpha: 1)
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    override var isHighlighted: Bool {
        didSet {
            cellImageView.tintColor = isHighlighted ? UIColor.white : #colorLiteral(red: 0.3694694042, green: 0.2306925356, blue: 0.6512844563, alpha: 1)
        }
    }

    
    override var isSelected: Bool {
        didSet {
            cellImageView.tintColor = isSelected ? UIColor.white : #colorLiteral(red: 0.3694694042, green: 0.2306925356, blue: 0.6512844563, alpha: 1)
        }
    }
    
    
    func setupViews() {
        
        addSubview(cellImageView)
        cellImageView.translatesAutoresizingMaskIntoConstraints = false
        cellImageView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant: 20).isActive = true
        cellImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        cellImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
        cellImageView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.28).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension CatalogMenuBar: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            delegate?.dressClick()
        case 1:
            delegate?.upperClick()
        case 2:
            delegate?.lowerClick()
        case 3:
            delegate?.shoesClick()
        default:
            delegate?.dressClick()
        }
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! menuBarCell
        
        cell.cellImageView.image = UIImage(named: menuCellIconNames[indexPath.item])?.withRenderingMode(.alwaysTemplate)
        cell.tintColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width / 4 , height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}



