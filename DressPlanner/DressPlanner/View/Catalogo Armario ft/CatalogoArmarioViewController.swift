//
//  CatalogoArmarioViewController.swift
//  DressPlanner
//
//  Created by Flavia Arsuffi on 22/10/19.
//  Copyright © 2019 Flavia Arsuffi. All rights reserved.
//

import UIKit
import Firebase


class CatalogoArmarioViewController: UIViewController {
    
    
    let defaults = UserDefaults.standard
    @IBOutlet weak var finalDateTextField: UITextField!
    @IBOutlet weak var userCatalogTableView: UITableView!
    
    private var datePicker: UIDatePicker?
    
    var arrayMain:[Produto] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //arrayMain = allClothesArray.filter{ $0.tipo == "Dresses" }
        observePosts(tipo: "Acessório")
        // TableView Setups
        self.userCatalogTableView.register(UINib(nibName: "ClothesCustomCell", bundle: nil), forCellReuseIdentifier: "ClothesCustomCell")
        self.userCatalogTableView.delegate = self
        self.userCatalogTableView.dataSource = self
        self.userCatalogTableView.tableFooterView = UIView()
        
        setupCatalogMenuBar()
    }
    
    let catalogMenuBar: CatalogMenuBar = {
        let menuBar = CatalogMenuBar()
        menuBar.translatesAutoresizingMaskIntoConstraints = false
        return menuBar
    }()
    
    func observePosts(tipo: String) {
        let email = defaults.string(forKey: "emaildefault") ?? ""
        let fEmail = email.replacingOccurrences(of: ".", with: ",")
        let postRef = Database.database().reference().child("users/\(fEmail)/clothesRegister")
        
        postRef.observe(.value) { (snapshot) in
            var tempPosts = [Produto]()
            
            for child in snapshot.children {
                if let childSnapshot = child as? DataSnapshot,
                    let dict = childSnapshot.value as? [String:Any],
                    let clothesName = dict["clothesName"] as? String,
                    let color = dict["color"] as? String,
                    let image = dict["image"] as? String,
                    let pieceType = dict["pieceType"] as? String,
                    let price = dict["price"] as? String,
                    let purchaseDate = dict["purchaseDate"] as? String,
                    let style = dict["style"] as? String{
                    
                    let produto = Produto(clothesName: clothesName, color: color, image: image, pieceType: pieceType, price: price, purchaseDate: purchaseDate, style: style)
                    
                    tempPosts.append(produto)
                }
            }
            self.arrayMain = tempPosts.filter{ $0.pieceType == tipo}
            self.userCatalogTableView.reloadData()
        }
    }
    
    private func setupCatalogMenuBar() {
        view.addSubview(catalogMenuBar)
        catalogMenuBar.backgroundColor = #colorLiteral(red: 0.5875301361, green: 0.3669178486, blue: 1, alpha: 1)
        
        catalogMenuBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        catalogMenuBar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        catalogMenuBar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        catalogMenuBar.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        catalogMenuBar.delegate = self
    }
}

extension CatalogoArmarioViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMain.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ClothesCustomCell? = tableView.dequeueReusableCell(withIdentifier: "ClothesCustomCell", for: indexPath) as? ClothesCustomCell
        cell?.setupCell(prod: arrayMain[indexPath.row])
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc =  self.storyboard?.instantiateViewController(withIdentifier: "ClothesDetailViewController") as? ClothesDetailViewController {
            vc.produto = arrayMain[indexPath.row]
            self.present(vc, animated: true, completion: nil)
        }
    }    
}

extension CatalogoArmarioViewController: CatalogMenuBarDelegate {
    
    func dressClick() {
        arrayMain.removeAll()
        observePosts(tipo: "Acessório")
    }
    
    func upperClick() {
        arrayMain.removeAll()
        observePosts(tipo: "Superior")
        print("Upper clothes tapped")
    }
    
    func lowerClick() {
        arrayMain.removeAll()
        observePosts(tipo: "Inferior")
        print("Lower clothes tapped")
    }
    
    func shoesClick() {
        arrayMain.removeAll()
        observePosts(tipo: "Calçado")
        print("shoes tapped")
    }
    
    
    
}
