//
//  ClothesDetailViewController.swift
//  DressPlanner
//
//  Created by Flavia Arsuffi on 18/11/19.
//  Copyright © 2019 Flavia Arsuffi. All rights reserved.
//

import UIKit

class ClothesDetailViewController: UIViewController {

    @IBOutlet weak var clotheImageView: UIImageView!
    
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var styleLabel: UILabel!
    @IBOutlet weak var purchaseDateLabel: UILabel!
    @IBOutlet weak var purcheseValueLabel: UILabel!
    
    @IBOutlet weak var corLabel: UILabel!
    @IBOutlet weak var tecidoLabel: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var valorLabel: UILabel!
    var produto: Produto?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        colorLabel.text = produto?.color ?? ""
        styleLabel.text = produto?.style ?? ""
        purchaseDateLabel.text = produto?.purchaseDate ?? ""
        purcheseValueLabel.text = produto?.price ?? ""
        
        guard let hasImage = produto?.image else {return }
        clotheImageView.image = base64ToImage((hasImage))
    }
    
    
    private func base64ToImage(_ base64String: String) -> UIImage? {
        guard let imageData = Data(base64Encoded: base64String) else { return nil }
        return UIImage(data: imageData)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
