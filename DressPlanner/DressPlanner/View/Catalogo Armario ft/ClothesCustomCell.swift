//
//  ClothesCustomCell.swift
//  DressPlanner
//
//  Created by Flavia Arsuffi on 23/10/19.
//  Copyright © 2019 Flavia Arsuffi. All rights reserved.
//

import UIKit

class ClothesCustomCell: UITableViewCell {

    @IBOutlet weak var clothesPhotoImageView: UIImageView!
    @IBOutlet weak var clothesTitleLabel: UILabel!
    @IBOutlet weak var clothesDetailLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(prod: Produto){
        clothesTitleLabel.text = "Nome: " + (prod.clothesName ?? "")
        clothesDetailLabel.text = "Cor: " + (prod.color ?? "")
        clothesPhotoImageView.image = base64ToImage(prod.image ?? "") ?? UIImage()
    }
    
    
    private func base64ToImage(_ base64String: String) -> UIImage? {
        guard let imageData = Data(base64Encoded: base64String) else { return nil }
        return UIImage(data: imageData)
    }
    
}
