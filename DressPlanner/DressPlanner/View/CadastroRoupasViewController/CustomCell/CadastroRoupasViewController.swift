//
//  CadastroRoupasViewController.swift
//  DressPlanner
//
//  Created by Flavia Arsuffi on 22/10/19.
//  Copyright © 2019 Regiane Castro. All rights reserved.
//

import UIKit
import FirebaseDatabase



class CadastroRoupasViewController: UIViewController {

    let userDefauts = UserDefaults.standard
    @IBOutlet weak var cadastroRoupasLabel: UILabel!
    @IBOutlet weak var acessorioBotao: UIButton!
    @IBOutlet weak var superiorBotao: UIButton!
    @IBOutlet weak var inferiorBotao: UIButton!
    @IBOutlet weak var calcadoBotao: UIButton!
    let tipoPeca: String = "pieceType"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cadastroRoupasLabel.font = UIFont.boldSystemFont(ofSize: 30.0)

    }
    
    @IBAction func clicouAcessorio(_ sender: Any) {
        clicouBotao()
        userDefauts.set("Acessório", forKey: tipoPeca)
    }
    
    @IBAction func clicouSuperior(_ sender: Any) {
        clicouBotao()
        userDefauts.set("Superior", forKey: tipoPeca)
    }
    
    @IBAction func clicouInferior(_ sender: Any) {
        clicouBotao()
        userDefauts.set("Inferior", forKey: tipoPeca)
    }
    
    @IBAction func clicouCalcado(_ sender: Any) {
        clicouBotao()
        userDefauts.set("Calçado", forKey: tipoPeca)
    }
    

    func clicouBotao() {
        let storyboard = UIStoryboard(name: "cadastroRoupas", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CadastroTodasRoupasViewController")
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
}
    
