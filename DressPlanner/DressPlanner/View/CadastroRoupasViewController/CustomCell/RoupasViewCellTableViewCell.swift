//
//  RoupasViewCellTableViewCell.swift
//  DressPlanner
//
//  Created by Regiane Castro on 04/12/19.
//  Copyright © 2019 Flavia Arsuffi. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase

protocol CadastroRoupasViewCellDelegate: class{
    func clicouNoBotaoImagem ()
    func currentImagem() -> UIImage
    func clicouVoltar()
}

class RoupasViewCellTableViewCell: UITableViewCell {
    
    weak var delegate: CadastroRoupasViewCellDelegate?
    @IBOutlet weak var pecaSelecionadaLabell: UILabel!
    @IBOutlet weak var pecaRoupaImageCell: UIImageView!
    @IBOutlet weak var selecionarCorRoupaLabel: UILabel!
    @IBOutlet weak var corRoupaPicker: UIPickerView!
    @IBOutlet weak var modeloRoupaSegmented: UISegmentedControl!
    @IBOutlet weak var nomeDaPecaTextField: UITextField!
    @IBOutlet weak var valorDaPecaTextField: UITextField!
    @IBOutlet weak var dataDaCompraTextField: UITextField!
    let defaults = UserDefaults.standard
    
    var currentPickerColor: String = "Branco"
    var dataSourceColor: [String] = []
    
    var estiloPeca: String = "liso"
    var imageSelected: UIImage?
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dataSourceColor = ["Branco", "Azul", "Verde", "Amarelo", "Roxo", "Vermelho", "Cinza", "Preto", "Rosa", "Laranja", "Marrom"]
        
        corRoupaPicker.dataSource = self
        corRoupaPicker.delegate = self
        
        dataDaCompraTextField.delegate = self
        valorDaPecaTextField.delegate = self
        
        self.nomeDaPecaTextField.delegate = self
        self.dataDaCompraTextField.delegate = self
        self.valorDaPecaTextField.delegate = self
        pecaRoupaImageCell.layer.cornerRadius = 25.0
        pecaRoupaImageCell.clipsToBounds = true
        initVariaveis()
    }
    
    func initVariaveis() {
        pecaSelecionadaLabell.text = defaults.string(forKey: "pieceType")
    }
    
    
    
    @IBAction func indexSegControl(_ sender: Any) {
        switch modeloRoupaSegmented.selectedSegmentIndex {
        case 0:
            estiloPeca = "liso"
        case 1:
            estiloPeca = "estampado"
        default:
            break
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        nomeDaPecaTextField.resignFirstResponder()
        valorDaPecaTextField.resignFirstResponder()
        dataDaCompraTextField.resignFirstResponder()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func setupImage(image: UIImage?) {
        if image != nil {
            self.pecaRoupaImageCell.image = image
        }
    }
    
    @IBAction func clicouBotao(_ sender: Any) {
        self.delegate?.clicouNoBotaoImagem()
    }
    
    
    @IBAction func clicouBotaoVoltar(_ sender: UIButton) {
        self.delegate?.clicouVoltar()
    }
    
    var alertaSalvarRoupas: UIViewController?
    
    @IBAction func salvarBotao(_ sender: UIButton) {
        
        let email = UserDefaults.standard.string(forKey: "emaildefault")
        let formattedEmail = email?.replacingOccurrences(of: ".", with: ",")
        
        imageSelected = self.delegate?.currentImagem()
        let imageStr = imageSelected?.jpegData(compressionQuality: 0.2)?.base64EncodedString()
        //        UIImage(data: Data(base64Encoded:     ))
        
        Database.database().reference().child("users/\(formattedEmail ?? "")").child("/clothesRegister").childByAutoId().setValue(["pieceType":pecaSelecionadaLabell.text ?? "", "clothesName": nomeDaPecaTextField.text ?? "", "color": currentPickerColor ?? "", "image": imageStr, "price": valorDaPecaTextField.text ?? "" , "purchaseDate": dataDaCompraTextField.text ?? "", "style": estiloPeca])
        
        
        let alert = UIAlertController(title: "Salvo", message: "Cadastro realizado com sucesso", preferredStyle: .actionSheet)
        
        let btnOK = UIAlertAction(title: "OK", style: .default) { (alert) in
        }
        
        
        alert.addAction(btnOK)
        
        alertaSalvarRoupas?.present(alert, animated: true, completion: nil)
        
    }
    
}

extension RoupasViewCellTableViewCell: UIPickerViewDataSource, UIPickerViewDelegate {
    
    
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSourceColor[row]
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSourceColor.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        return currentPickerColor = dataSourceColor[row]
    }
    
    func formattedCurrency(textField: UITextField) {
        let myDouble = Double(valorDaPecaTextField.text ?? "0") ?? 0
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "pt_BR")

        let priceString = currencyFormatter.string(from: NSNumber(value: myDouble))!
        textField.text = priceString
        print(priceString)
    }
}

extension RoupasViewCellTableViewCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == valorDaPecaTextField  {
            self.valorDaPecaTextField.text = self.valorDaPecaTextField.text?.replacingOccurrences(of: ",", with: ".")
            self.formattedCurrency(textField: valorDaPecaTextField)
        }
    }
    
}
