//
//  CadastroTodasRoupasViewController.swift
//  DressPlanner
//
//  Created by Regiane Castro on 04/12/19.
//  Copyright © 2019 Flavia Arsuffi. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase


class CadastroTodasRoupasViewController: UIViewController {
    @IBOutlet weak var acessorioButton: UIButton!
    @IBOutlet weak var roupaTableView: UITableView!
    var controller: CadastroRoupasController?
    var imageSelected: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        
        self.roupaTableView.register(UINib(nibName: "RoupasViewCellTableViewCell", bundle: nil), forCellReuseIdentifier: "RoupasViewCellTableViewCell")
        
        self.roupaTableView.dataSource = self
        self.roupaTableView.delegate = self
        
        self.controller = CadastroRoupasController()
        //        self.controller?.delegate = self as! ProdutoControllerDelegate
        
    }
    
    
    @IBAction func voltarBotao(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
}

extension CadastroTodasRoupasViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.controller?.numberOfRowsInSection() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: RoupasViewCellTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "RoupasViewCellTableViewCell", for: indexPath) as? RoupasViewCellTableViewCell
        
        cell?.delegate = self
        cell?.alertaSalvarRoupas = self
        
        if #available(iOS 13.0, *) {
            cell?.setupImage(image: imageSelected)
        } else {
            // Fallback on earlier versions
        }
    
        return cell ?? UITableViewCell()
    }
    
}

extension CadastroTodasRoupasViewController: CadastroRoupasViewCellDelegate {    

    func clicouVoltar() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func currentImagem() -> UIImage {
        return imageSelected ?? UIImage()
    }
    
    func clicouNoBotaoImagem () {
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Câmera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Álbum de Fotos", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
        
            
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
}

extension CadastroTodasRoupasViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imageSelected = image
        self.roupaTableView.reloadData()
        self.dismiss(animated: true, completion: nil)
        
    }
}


