//  DressPlanner
//
//  Created by Alessandro Souza de Oliveira on 05/11/19.
//  Copyright © 2019 Flavia Arsuffi. All rights reserved.
//

import UIKit
import Firebase

class SignUpController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var telephoneTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        }
    
        @objc func dismissKeyboard() {
            view.endEditing(true)
        
    }
    @IBAction func signUp(_ sender: UIButton) {
        guard let email = emailTextField.text else {return}
        guard let password = pwdTextField.text else {return}
        
        createUser(email: email, password: password)
    }
    

    
    //retirar ao criar a controller, pois devemos reutilizar o mesmo código
    func signOut() {
        do {
            try Auth.auth().signOut()
            let storyboard = UIStoryboard(name: "login", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "login")
            self.present(controller, animated: true, completion: nil)
        } catch let error {
            print("Falha ao deslogar", error)
        }
    }
    
    func createUser(email: String, password: String) {
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                if let error = error {
                    let alert = UIAlertController(title: "Alerta", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
            
            
//            result?.user.createProfileChangeRequest().displayName = "Alessandro"
            
            let emailUser = ["email" : email]
            let fullNameUser = ["fullName" : self.fullNameTextField.text ?? ""]
            let telephoneUser = ["telephone" : self.telephoneTextField.text ?? ""]
            let fEmail = email.replacingOccurrences(of: ".", with: ",")
            
            self.updateUser(child: fEmail, value: emailUser)
            self.updateUser(child: fEmail, value: fullNameUser)
            self.updateUser(child: fEmail, value: telephoneUser)
            
//            let alert = UIAlertController(title: "Alerta", message: "Usuário cadastrado com sucesso!!!", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
            
            self.signOut()
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    func updateUser(child: String, value: [String:String]) {
        
        Database.database().reference().child("users").child(child).updateChildValues(value) { (error, ref) in
            if let error = error {
                print("Falha ao atualizar o database", error.localizedDescription)
                return
            }
        
        }
    }

}
