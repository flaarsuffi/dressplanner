//
//  LoginController.swift
//  DressPlanner
//
//  Created by Alessandro Souza de Oliveira on 05/11/19.
//  Copyright © 2019 Flavia Arsuffi. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class LoginController: UIViewController, GIDSignInDelegate {
    
    func bora() {
        self.authenticateUserAndConfigureVIew()
    }
    
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    let defaults = UserDefaults.standard
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
         GIDSignIn.sharedInstance()?.presentingViewController = self
         
         // Automaticamente restaura o último usuário logado
         GIDSignIn.sharedInstance()?.restorePreviousSignIn()
         GIDSignIn.sharedInstance().delegate = self
         
        emailTextField.delegate = self
        pwdTextField.delegate = self
        styleCustomButton()
         authenticateUserAndConfigureVIew()
    }
    
    func styleCustomButton() {
        googleButton.layer.cornerRadius = 10
        loginButton.layer.cornerRadius = 10
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
            } else {
                print("\(error.localizedDescription)")
            }
            // [START_EXCLUDE silent]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
            // [END_EXCLUDE]
            return
        }
        // Perform any operations on signed in user here.
        //        let userId = user.userID                  // For client-side use only!
        //        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        //        let givenName = user.profile.givenName
        //        let familyName = user.profile.familyName
        //        let email = user.profile.email
        //
        
        
        
        let fEmail = user.profile.email.replacingOccurrences(of: ".", with: ",")
        let emailUser = ["email" : user.profile.email ?? ""]
        let fullNameUser = ["fullName" : user.profile.name ?? ""]
        
        self.updateUser(child: fEmail, value: emailUser)
        self.updateUser(child: fEmail, value: fullNameUser)
        
        let defaults = UserDefaults.standard
        
        setUserData(email: user.profile.email)
        
        
        if user.profile.hasImage {
            defaults.set(user.profile.imageURL(withDimension: 400), forKey: "image")
        }
        
        goToHome()
        
        
        // [START_EXCLUDE]
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "ToggleAuthUINotification"),
            object: nil,
            userInfo: ["statusText": "Signed in user:\n\(fullName!)"])
        // [END_EXCLUDE]
        
        guard let authentication = user.authentication else {return}
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential) { (result, error) in
            
            //self.window
        }
    }
    
    
    func updateUser(child: String, value: [String:String]) {
        
        Database.database().reference().child("users").child(child).updateChildValues(value) { (error, ref) in
            if let error = error {
                print("Falha ao atualizar o database", error.localizedDescription)
                return
            }
        
        }
    }
    
    
    
    
    
    
    
    
    //LOGIN COM EMAIL
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // [START_EXCLUDE]
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "ToggleAuthUINotification"),
            object: nil,
            userInfo: ["statusText": "User has disconnected."])
        // [END_EXCLUDE]
    }
    
    
    
    @IBAction func signInUserWithGoogle(_ sender: UIButton) {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func signIn(_ sender: UIButton) {
        guard let email = emailTextField.text else {return}
        guard let password = pwdTextField.text else {return}
        
        logUserIn(email: email, password: password)
    }
    
    
    func setUserData(email: String) {
        let fEmail = email.replacingOccurrences(of: ".", with: ",")
        let postRef = Database.database().reference().child("users/\(fEmail)")
        
        postRef.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            let fullName = value?["fullName"] as? String ?? ""
            let email = value?["email"] as? String ?? ""
            
            self.defaults.set(fullName, forKey: "fullname")
            self.defaults.set(email, forKey: "emaildefault")
        }
    }
    
    func logUserIn(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error {
                let alert = UIAlertController(title: "Alerta", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.setUserData(email: email)
            self.authenticateUserAndConfigureVIew()
        }
        
    }
    
    func authenticateUserAndConfigureVIew() {
        if  defaults.string(forKey: "fullname") != nil {
            goToHome()
        }
        guard let _ = Auth.auth().currentUser else {
            return
        }
        goToHome()
    }
    
    
    func goToHome() {
        let storyboard = UIStoryboard(name: "tabBarController", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "tabbar") as? UITabBarController, let _ = UIApplication.shared.delegate as? AppDelegate  else {return}
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        self.present(controller, animated: false, completion: nil)
    }
}

extension LoginController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            pwdTextField.becomeFirstResponder()
        } else if textField == pwdTextField {
            self.signIn(UIButton())
        }
        
        return true
    }
}
