//
//  NewsTableViewCell.swift
//  DressPlanner
//
//  Created by Vitor G. Abreu on 28/11/19.
//  Copyright © 2019 Flavia Arsuffi. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewCell: UIImageView!
    
    @IBOutlet weak var labelViewCell: UILabel!
    
    @IBOutlet weak var newsView: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        imageViewCell.layer.cornerRadius = 25.0
        imageViewCell.clipsToBounds = true
        
        newsView.layer.cornerRadius = 25.0
        newsView.clipsToBounds = true
    }

}
