//
//  PerfilViewController.swift
//  DressPlanner
//
//  Created by Alessandro Souza de Oliveira on 21/11/19.
//  Copyright © 2019 Flavia Arsuffi. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import SDWebImage


class PerfilViewController: UIViewController {

    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var logoffButton: UIButton!
    let defaults = UserDefaults.standard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customImageView()
        initViews()
        loadImage()
        styleCustomButton()
        perform(#selector(initViews), with: nil, afterDelay: 5)
        
    }
    
    @objc func initViews() {
        self.emailLabel.text = defaults.string(forKey: "emaildefault")
        self.fullNameLabel.text = defaults.string(forKey: "fullname")
    }

    
    func customImageView() {
        imageView.layer.masksToBounds = false
        imageView.layer.borderWidth = 3.0
        imageView.layer.borderColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1).cgColor
        imageView.layer.cornerRadius = imageView.frame.size.height/2
        imageView.clipsToBounds = true
    }
    
    
    @IBAction func logoff(_ sender: UIButton) {
        singOut()
    }
    
    func singOut() {
        do {
            try
            Auth.auth().signOut()
            GIDSignIn.sharedInstance().signOut()
            defaults.removeObject(forKey: "emaildefault")
            defaults.removeObject(forKey: "fullname")
            defaults.removeObject(forKey: "image")
            self.dismiss(animated: true)
        } catch let error {
            print("Falha ao deslogar", error)
        }
    }
    
    func removeUserDefaults() {
        defaults.removeObject(forKey: "fullname")
        
    }
    
    func styleCustomButton() {
        logoffButton.layer.cornerRadius = 25
    }
    
    func loadImage() {
        
        if defaults.url(forKey: "image") != nil {
            let image = defaults.url(forKey: "image")
            self.imageView.sd_setImage(with: image, completed: nil)
        }else {
            imageView.image = UIImage(named: "userDefault")
        }

    }
}


