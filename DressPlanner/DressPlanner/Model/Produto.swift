//
//  Produto.swift
//  DressPlanner
//
//  Created by Regiane Castro 12/11/19.
//  Copyright © 2019 Regiane Castro. All rights reserved.
//

import Foundation
import UIKit
//enum NomeCadastro {
//    case vestido
//    case superior
//    case inferior
//    case sapatos
//    
//    func cadastrarNome(nome: NomeCadastro) -> String? {
//        switch nome {
//        case.vestido:
//            return "Vestido"
//        case .superior:
//            return "Superior"
//        case .inferior:
//            return "Inferior"
//        case .sapatos:
//            return "Sapatos"
//        }
//    }
//}


enum tipoCadastro {
    case vestimenta
    case calcados
    
    func cadastrarTipo(nome: tipoCadastro) -> [String] {
        switch nome {
        case.vestimenta:
            return ["Liso", "Estampado"]
        case .calcados:
            return ["Esportivo", "Casual", "Social"]
        }
    }
}



struct Produto {
    var clothesName: String?
    var color: String?
    var image: String?
    var pieceType: String?
    var price: String?
    var purchaseDate: String?
    var style: String?
}

typealias Roupas = [Produto]
